Format: 3.0 (quilt)
Source: axd-archive-keyring
Binary: axd-archive-keyring
Architecture: all
Version: 2023.11.25-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/axd-archive-keyring
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), gnupg
Package-List:
 axd-archive-keyring deb misc important arch=all
Checksums-Sha1:
 651b4486f5ca94b08d28e184eb2d8c3393798c04 4221 axd-archive-keyring_2023.11.25.orig.tar.gz
 a0b4703366270b5611ae98ed51e3b05f96d7f055 1276 axd-archive-keyring_2023.11.25-1.debian.tar.xz
Checksums-Sha256:
 884b07db67c5514b3752bbcbac4a0354bab2e292a2d81a43d2d1c423fe0f5cb1 4221 axd-archive-keyring_2023.11.25.orig.tar.gz
 ae2200e35b3a4d69beb52574d1b175f7c2b02f8569201bcc841ac233fb6dbfa0 1276 axd-archive-keyring_2023.11.25-1.debian.tar.xz
Files:
 bdc0c8656cd11d3a222f3eb1958eb7bd 4221 axd-archive-keyring_2023.11.25.orig.tar.gz
 245a159880d0530ed0d787b9cd03a16e 1276 axd-archive-keyring_2023.11.25-1.debian.tar.xz
