Format: 3.0 (quilt)
Source: apt-axd
Binary: apt-axd
Architecture: all
Version: 1.0.0-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/apt-axd
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13)
Package-List:
 apt-axd deb system required arch=all
Checksums-Sha1:
 38e6938f2cd2858a2b0b085c76c3372fb0305ba9 1803 apt-axd_1.0.0.orig.tar.gz
 edfbbab4877a1ec9f6a884687f39ba64f6bb9501 1208 apt-axd_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 2b48cfd0f42011613b4007e65b0bff1a50f7c7ef90fc44457d5ff68fb67dcb0c 1803 apt-axd_1.0.0.orig.tar.gz
 e358642a741477504b4832886c687dae4351c00e3b4ad1ca23a9ce818627ab45 1208 apt-axd_1.0.0-1.debian.tar.xz
Files:
 d7ede7d0d15c5cf2ea24f4d0ade8275d 1803 apt-axd_1.0.0.orig.tar.gz
 1b38a4259e10c2a6716b949f0d244880 1208 apt-axd_1.0.0-1.debian.tar.xz
