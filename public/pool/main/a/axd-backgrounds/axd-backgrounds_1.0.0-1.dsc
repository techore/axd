Format: 3.0 (quilt)
Source: axd-backgrounds
Binary: axd-backgrounds
Architecture: any
Version: 1.0.0-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/axd-backgrounds
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 axd-backgrounds deb x11 optional arch=any
Checksums-Sha1:
 5a64ac923d6891c78580b56ee811275e27868792 13085732 axd-backgrounds_1.0.0.orig.tar.gz
 41a3557b1916c06110c103391e33bcd3f64f62bc 1612 axd-backgrounds_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 97240186e0167b24231412632271ee4203cb0735bd1b984fd3d5d20aed180667 13085732 axd-backgrounds_1.0.0.orig.tar.gz
 870fc58074e3eacd3318cc2645f17960bcee35431278632e31877d9ecdbe20bc 1612 axd-backgrounds_1.0.0-1.debian.tar.xz
Files:
 2e0f36ea3eabd8890b323dcdddff5b8a 13085732 axd-backgrounds_1.0.0.orig.tar.gz
 fe39c52c123e06705ff1479705641600 1612 axd-backgrounds_1.0.0-1.debian.tar.xz
