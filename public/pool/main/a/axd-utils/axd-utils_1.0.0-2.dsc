Format: 3.0 (quilt)
Source: axd-utils
Binary: axd-utils
Architecture: all
Version: 1.0.0-2
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/axd-utils
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13)
Package-List:
 axd-utils deb utils required arch=all
Checksums-Sha1:
 119dde5120483488a2dd670810b0f956966e7524 5758 axd-utils_1.0.0.orig.tar.gz
 df1070519361e5a539cb699c482e110e5d467349 1460 axd-utils_1.0.0-2.debian.tar.xz
Checksums-Sha256:
 82acf443146595f497d3371742ea897eede217be6aa71aee7f70745cfed8168f 5758 axd-utils_1.0.0.orig.tar.gz
 f4cb4f20d717ca06974be7fbb466e0826199ce99fff99dd0eeea9ddfb31ba803 1460 axd-utils_1.0.0-2.debian.tar.xz
Files:
 2e21121dd75ad9e7ae4f560bf91129f3 5758 axd-utils_1.0.0.orig.tar.gz
 d7fecb592459c93d6d74a9a17a6662d5 1460 axd-utils_1.0.0-2.debian.tar.xz
