Format: 3.0 (quilt)
Source: zsh-antidote
Binary: zsh-antidote
Architecture: all
Version: 1.9.6-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://github.com/mattmc3/antidote
Standards-Version: 4.6.2
Vcs-Browser: https://gitlab.com/techore/zsh-antidote
Vcs-Git: https://gitlab.com/techore/zsh-antidote.git
Build-Depends: debhelper-compat (= 13), pandoc, zsh
Package-List:
 zsh-antidote deb shells optional arch=all
Checksums-Sha1:
 86a5551c108ce09a1e99fc5ced684a40ae3fcee1 41620 zsh-antidote_1.9.6.orig.tar.gz
 eb4eda41f37888ac3388ba2ffb03d9fedd814a40 2704 zsh-antidote_1.9.6-1.debian.tar.xz
Checksums-Sha256:
 17b76964b2faebb750c0291effc452aaab09a13db16c5fa8971db8454e24f918 41620 zsh-antidote_1.9.6.orig.tar.gz
 a4f39def8c2af07bc61ef5f5ec68b78d09233d83a3433fc12d0e07d46ee02102 2704 zsh-antidote_1.9.6-1.debian.tar.xz
Files:
 2ff83feebb1409a37ede5606b75ffe7f 41620 zsh-antidote_1.9.6.orig.tar.gz
 7f8ee6f27f55ff44d2aec1218452e277 2704 zsh-antidote_1.9.6-1.debian.tar.xz
