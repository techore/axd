Format: 3.0 (quilt)
Source: keybindhelp
Binary: keybindhelp
Architecture: all
Version: 1.0.0-5
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/keybindhelp
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13)
Package-List:
 keybindhelp deb x11 optional arch=all
Checksums-Sha1:
 83079919a35bfa5a78989b3fb58b384d3967d113 3391 keybindhelp_1.0.0.orig.tar.gz
 af44bb78574754eb3a8b09602133deaf4f1ca724 1444 keybindhelp_1.0.0-5.debian.tar.xz
Checksums-Sha256:
 f5c93eb56a54d821fad6c0085f4ed0c3bd034eec8a8ccdafd06029bb644c5888 3391 keybindhelp_1.0.0.orig.tar.gz
 e797b323ed5872918984ba693dedaf7c7957700ced71de12159bcb3bc486dc56 1444 keybindhelp_1.0.0-5.debian.tar.xz
Files:
 2da743043f306a98409b6f1a446fafdd 3391 keybindhelp_1.0.0.orig.tar.gz
 b299653d275ed8e0dc7556a1a2642970 1444 keybindhelp_1.0.0-5.debian.tar.xz
