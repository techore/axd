Format: 3.0 (quilt)
Source: greetd-runit
Binary: greetd-runit
Architecture: any
Version: 1.0.0-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/greetd-runit
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 greetd-runit deb x11 optional arch=any
Checksums-Sha1:
 d1a909bcc694367f51e3996f2676b3512cf07bd4 618 greetd-runit_1.0.0.orig.tar.gz
 ef7fd442ab9232458cbbe262fbe67759111c03ef 1576 greetd-runit_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 16c990c65c335a6dcddb0a3a22dcb7b43dce5cf2694b92a97799f06dcfab7c8a 618 greetd-runit_1.0.0.orig.tar.gz
 c3ad9a2f90d857dee5e31d940881772b751383cd8a09a8e80acb5fa630f2d0ef 1576 greetd-runit_1.0.0-1.debian.tar.xz
Files:
 5bf6ef015f101907dbf93bcb94bf48fa 618 greetd-runit_1.0.0.orig.tar.gz
 871f1f0f09faf2ee1a81e68db93f462d 1576 greetd-runit_1.0.0-1.debian.tar.xz
