Format: 3.0 (quilt)
Source: desktop-techore-axd
Binary: desktop-techore-axd
Architecture: any
Version: 1.0.0-2
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/desktop-techore-axd
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 desktop-techore-axd deb x11 optional arch=any
Checksums-Sha1:
 20ea8045f2041efa1ec0faa6e9f924ee868a2ad5 1977 desktop-techore-axd_1.0.0.orig.tar.gz
 7b953862eca3cd4289e8acc0b204fe069ddf8cbb 1348 desktop-techore-axd_1.0.0-2.debian.tar.xz
Checksums-Sha256:
 8869ac0d0543b9d376d900516fe393fb17a59baab80b575a4e3a9681259f0f32 1977 desktop-techore-axd_1.0.0.orig.tar.gz
 45494c53136d28b29f38712116f17ac57da07255386e6e94cd5bdfc48f7ca486 1348 desktop-techore-axd_1.0.0-2.debian.tar.xz
Files:
 6280cf43d9d2605027cf7970d79d41fb 1977 desktop-techore-axd_1.0.0.orig.tar.gz
 11fef728f17c79dfab1489ca24e69dfa 1348 desktop-techore-axd_1.0.0-2.debian.tar.xz
