Format: 3.0 (quilt)
Source: dragon
Binary: dragon
Architecture: any
Version: 0.0+git20221016.0a56eb2-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/dragon
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libgtk-3-dev
Package-List:
 dragon deb x11 optional arch=any
Checksums-Sha1:
 1777f091255dd8ce7fbb4250469c0a8a5be11098 50927 dragon_0.0+git20221016.0a56eb2.orig.tar.gz
 4c8be9d79cc006486a3136d8459f256230843ba7 1668 dragon_0.0+git20221016.0a56eb2-1.debian.tar.xz
Checksums-Sha256:
 d52f113a34ec929c48305f30a640726fe8e5e05eb4f0b6658aba0e99bc80ae05 50927 dragon_0.0+git20221016.0a56eb2.orig.tar.gz
 9cb9b52474922ddf8e3233cdab6d5d8e0d23db388ed91571bfe8b1962f848e5f 1668 dragon_0.0+git20221016.0a56eb2-1.debian.tar.xz
Files:
 edf08ecfe4f4df0c7206ccb76ed62afc 50927 dragon_0.0+git20221016.0a56eb2.orig.tar.gz
 dd17f3dc7d0053a82ed919ce5f14864e 1668 dragon_0.0+git20221016.0a56eb2-1.debian.tar.xz
