Format: 3.0 (quilt)
Source: dusk
Binary: dusk
Architecture: any
Version: 0.0+git20240430.83206c1-3
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/dusk
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libx11-dev, libx11-xcb-dev, libxcb-res0-dev, libxft-dev, libimlib2-dev, libyajl-dev, libxinerama-dev, libxrandr-dev, libxfixes-dev, libxi-dev, libfribidi-dev
Package-List:
 dusk deb x11 optional arch=any
Checksums-Sha1:
 7358735b7a06dabc1b037fecbd361922b9cec552 149338 dusk_0.0+git20240430.83206c1.orig.tar.gz
 30d9465bd48a32dae53912e27923fc5b83293f6c 11056 dusk_0.0+git20240430.83206c1-3.debian.tar.xz
Checksums-Sha256:
 de0ce7c5f52f9c026252645a24cca93dc72a05d20ed6f18d136d2f6c9bbfcb88 149338 dusk_0.0+git20240430.83206c1.orig.tar.gz
 06f6b6e0ab34b2ea813bc751868f9c60df4dcac7b7e3874248e5bb54763d646c 11056 dusk_0.0+git20240430.83206c1-3.debian.tar.xz
Files:
 ea8c7d1760d82c6769f4f0aa1b2cc8b2 149338 dusk_0.0+git20240430.83206c1.orig.tar.gz
 26b5487f9f76bdaaa66ad20186809885 11056 dusk_0.0+git20240430.83206c1-3.debian.tar.xz
