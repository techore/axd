Format: 3.0 (quilt)
Source: desktop-defaults-axd
Binary: desktop-defaults-axd
Architecture: any
Version: 1.0.0-12
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/desktop-defaults-axd
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 desktop-defaults-axd deb x11 optional arch=any
Checksums-Sha1:
 3d58bd41e11285f2426b83dc6d4c1d7c154c7d5f 185836 desktop-defaults-axd_1.0.0.orig.tar.gz
 853722bcc48a9bbfaf466792d5528fe288c83704 2036 desktop-defaults-axd_1.0.0-12.debian.tar.xz
Checksums-Sha256:
 a24a4537ddb50c967e6c7673f0f53e7d5da9b9ea9b024be63a8862ebd8596ecc 185836 desktop-defaults-axd_1.0.0.orig.tar.gz
 656bc133a68102d023986282c78363dd2a5589666684614c9c200d1fcc7f8af6 2036 desktop-defaults-axd_1.0.0-12.debian.tar.xz
Files:
 302b860142f3da7de4d3a8f0d9ebf4f9 185836 desktop-defaults-axd_1.0.0.orig.tar.gz
 ae592f1b63d37227a853b1452fc2b832 2036 desktop-defaults-axd_1.0.0-12.debian.tar.xz
