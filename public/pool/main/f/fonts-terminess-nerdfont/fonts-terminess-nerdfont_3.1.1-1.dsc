Format: 3.0 (quilt)
Source: fonts-terminess-nerdfont
Binary: fonts-terminess-nerdfont
Architecture: all
Version: 3.1.1-1
Maintainer: techore <axdwm@pm.me>
Homepage: https://github.com/ryanoasis/nerd-fonts
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13)
Package-List:
 fonts-terminess-nerdfont deb fonts optional arch=all
Checksums-Sha1:
 6b337afed0b854cc7079ee6ace2b72fefe6a559a 1942492 fonts-terminess-nerdfont_3.1.1.orig.tar.xz
 bda6da8eab5eef02201b04322a8ef897c5110744 3716 fonts-terminess-nerdfont_3.1.1-1.debian.tar.xz
Checksums-Sha256:
 4af85f352ca7af0f91c60794a10333d4df60c441bcd60c1d8071029d13a8344c 1942492 fonts-terminess-nerdfont_3.1.1.orig.tar.xz
 8d7c46e6e9d27a5429d8cb30f6379feddeacf46433654b30b898f25be20c1af8 3716 fonts-terminess-nerdfont_3.1.1-1.debian.tar.xz
Files:
 605b2338082e0b2ddee7a10938f8c2ad 1942492 fonts-terminess-nerdfont_3.1.1.orig.tar.xz
 d91165cbcbee2baa4e97fbd5d20aacbc 3716 fonts-terminess-nerdfont_3.1.1-1.debian.tar.xz
