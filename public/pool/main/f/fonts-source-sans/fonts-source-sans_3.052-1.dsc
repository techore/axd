Format: 3.0 (quilt)
Source: fonts-source-sans
Binary: fonts-source-sans
Architecture: all
Version: 3.052-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://github.com/adobe-fonts/source-code-pro
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), wget
Package-List:
 fonts-source-sans deb fonts optional arch=all
Checksums-Sha1:
 0f83215e975c46b3d7fdb674591d0e1c870875c6 2399027 fonts-source-sans_3.052.orig.tar.gz
 66b46b9e5ea35368fce3be4a3e6492a3bb3e176c 3164 fonts-source-sans_3.052-1.debian.tar.xz
Checksums-Sha256:
 263f14105468e0b177a385326e709246f8f9e2c485fc6e62701029c91d02ca22 2399027 fonts-source-sans_3.052.orig.tar.gz
 d756ac36d10cba71fdb186b0a359125ace3d39827c180b31744a2a0ce4731f8e 3164 fonts-source-sans_3.052-1.debian.tar.xz
Files:
 6a4ba322bde1f0a273954c3206fc72f2 2399027 fonts-source-sans_3.052.orig.tar.gz
 37c55d419df05f1e6e63f3c99f0b9e7e 3164 fonts-source-sans_3.052-1.debian.tar.xz
