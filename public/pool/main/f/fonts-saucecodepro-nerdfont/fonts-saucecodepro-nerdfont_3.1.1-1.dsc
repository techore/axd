Format: 3.0 (quilt)
Source: fonts-saucecodepro-nerdfont
Binary: fonts-saucecodepro-nerdfont
Architecture: all
Version: 3.1.1-1
Maintainer: techore <axdwm@pm.me>
Homepage: https://github.com/ryanoasis/nerd-fonts
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13)
Package-List:
 fonts-saucecodepro-nerdfont deb fonts optional arch=all
Checksums-Sha1:
 536333e5aba62d86b287196802ec893f85d44bc4 2413132 fonts-saucecodepro-nerdfont_3.1.1.orig.tar.xz
 a4052a35a297ef8682ffc9e63435272203e58759 3616 fonts-saucecodepro-nerdfont_3.1.1-1.debian.tar.xz
Checksums-Sha256:
 13522f0de117f7dd52b55ef945b33a571fdd697ff6f8bcb5292993d7e593bd3a 2413132 fonts-saucecodepro-nerdfont_3.1.1.orig.tar.xz
 e40acf66be5839b950e9553d1fa94a8b5c52878eede12b07f3f6c7d684b82f14 3616 fonts-saucecodepro-nerdfont_3.1.1-1.debian.tar.xz
Files:
 40ac666fd8797d8dd704c1f6bec6df2d 2413132 fonts-saucecodepro-nerdfont_3.1.1.orig.tar.xz
 8344014647f635587ba030e309612da1 3616 fonts-saucecodepro-nerdfont_3.1.1-1.debian.tar.xz
