Format: 3.0 (quilt)
Source: fonts-material-icons
Binary: fonts-material-icons
Architecture: all
Version: 4.0.0-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://github.com/google/material-design-icons
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), wget
Package-List:
 fonts-material-icons deb fonts optional arch=all
Checksums-Sha1:
 eb116e7e463d7303fba5e20590e00543138fc4ab 672616 fonts-material-icons_4.0.0.orig.tar.gz
 94ca8b17668993f08c1017bb478faeb2bb51bea7 1612 fonts-material-icons_4.0.0-1.debian.tar.xz
Checksums-Sha256:
 c1fe4a8736fa59b0ee6ae46e037ec67e2d3e691ffbbcb90270844bff4c76fe01 672616 fonts-material-icons_4.0.0.orig.tar.gz
 233467c59c4745529337308cc5791eed2aeb5f701bdd1c83dbf3f78a016c78da 1612 fonts-material-icons_4.0.0-1.debian.tar.xz
Files:
 cbda9151835e00199a6b38d79fc5f2a0 672616 fonts-material-icons_4.0.0.orig.tar.gz
 414e9181b220e55df8a9e7a0fcbbe352 1612 fonts-material-icons_4.0.0-1.debian.tar.xz
