Format: 3.0 (quilt)
Source: nordic-theme
Binary: nordic-theme
Architecture: all
Version: 1.0.0-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://github.com/EliverLara/Nordic
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), wget
Package-List:
 nordic-theme deb x11 optional arch=all
Checksums-Sha1:
 f5846a349d19021fe7d9d501bcef0df645c1d76e 492077 nordic-theme_1.0.0.orig.tar.gz
 e347c55a3d3b9f3aa842606624381dbb3ca916de 1380 nordic-theme_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 1d49775c475a52e706ea34f61b4d02db6be32df28405276b34795266beeb152b 492077 nordic-theme_1.0.0.orig.tar.gz
 53e0cfe2e6dfba58b524d731fe344812e6c95a3355dd99f7a3f31735b9be96c3 1380 nordic-theme_1.0.0-1.debian.tar.xz
Files:
 e55d9f18abe81b46b1a77f001b456a2e 492077 nordic-theme_1.0.0.orig.tar.gz
 00522560057d28503b7f72139c4e7785 1380 nordic-theme_1.0.0-1.debian.tar.xz
