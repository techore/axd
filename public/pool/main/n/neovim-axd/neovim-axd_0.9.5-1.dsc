Format: 3.0 (quilt)
Source: neovim-axd
Binary: neovim-axd, neovim-runtime-axd
Architecture: any all
Version: 0.9.5-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://neovim.io/
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), wget
Package-List:
 neovim-axd deb editors optional arch=any
 neovim-runtime-axd deb editors optional arch=all
Checksums-Sha1:
 05103ca6ffc82c1313f1dd148cbb1f8c7c7a9e33 26686171 neovim-axd_0.9.5.orig.tar.gz
 57a9019e477a4299bfdf45d9809f3fd2cead6371 8484 neovim-axd_0.9.5-1.debian.tar.xz
Checksums-Sha256:
 c68a16ecc56dce785cd6f470b4d9112f1cbe81db7ce3732e1863de6268db311f 26686171 neovim-axd_0.9.5.orig.tar.gz
 2bab08868b3764bc7de5604dd9e05628e754790d5dbeca436e292c3cf63ca8c2 8484 neovim-axd_0.9.5-1.debian.tar.xz
Files:
 61b87ed1150ba656701fb1e6b12935c6 26686171 neovim-axd_0.9.5.orig.tar.gz
 f2a47361de50e46633c1eaf0ffae6abb 8484 neovim-axd_0.9.5-1.debian.tar.xz
