Format: 3.0 (quilt)
Source: nordzy-cursor-theme
Binary: nordzy-cursor-theme
Architecture: all
Version: 0.6.0-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/nordzy-cursor-theme
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13)
Package-List:
 nordzy-cursor-theme deb misc optional arch=all
Checksums-Sha1:
 e10e123c8947fe24612f36692792edac48447bf8 2861232 nordzy-cursor-theme_0.6.0.orig.tar.gz
 b008413ab82ee553cdc64daf7fa27a83abac08eb 1324 nordzy-cursor-theme_0.6.0-1.debian.tar.xz
Checksums-Sha256:
 a0baa6d598b9d4136248fb8cdc51a53ba6fb6feaaac0682d0f4d0d5eec5ba401 2861232 nordzy-cursor-theme_0.6.0.orig.tar.gz
 11f8bc35d4b924f1b3f08addc0fca9106896aedff115de751142ef771e2938ff 1324 nordzy-cursor-theme_0.6.0-1.debian.tar.xz
Files:
 cc0a8687761fb2d1dd45ea92f46f845d 2861232 nordzy-cursor-theme_0.6.0.orig.tar.gz
 48371a3d87d410265905f5891b1e72fd 1324 nordzy-cursor-theme_0.6.0-1.debian.tar.xz
