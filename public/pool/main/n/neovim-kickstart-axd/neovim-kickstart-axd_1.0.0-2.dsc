Format: 3.0 (quilt)
Source: neovim-kickstart-axd
Binary: neovim-kickstart-axd
Architecture: any
Version: 1.0.0-2
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/neovim-kickstart-axd
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 neovim-kickstart-axd deb x11 optional arch=any
Checksums-Sha1:
 e4b9f14bfdf83970885d3adc60814d6998a38ca8 18129 neovim-kickstart-axd_1.0.0.orig.tar.gz
 58527363533dd283b1bc660a09a85207b935c82e 1324 neovim-kickstart-axd_1.0.0-2.debian.tar.xz
Checksums-Sha256:
 8a4eb07f60759e90a9eceab452dc7185117b25f442f5eec84c70d1e3430e16b9 18129 neovim-kickstart-axd_1.0.0.orig.tar.gz
 62ea1273d72cc173c1851f654152a04fd22469fad4901cc8a841b63a4e093eb4 1324 neovim-kickstart-axd_1.0.0-2.debian.tar.xz
Files:
 9fc2234949e80eb7ef4bd59b83034e06 18129 neovim-kickstart-axd_1.0.0.orig.tar.gz
 ced3d7a7695f07b8a3bb889bdb2e09f4 1324 neovim-kickstart-axd_1.0.0-2.debian.tar.xz
