Format: 3.0 (quilt)
Source: papirus-icon-theme
Binary: papirus-icon-theme
Architecture: all
Version: 20240411-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
Standards-Version: 4.6.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 papirus-icon-theme deb x11 optional arch=all
Checksums-Sha1:
 a366f2a214792479e3dd7a704615c82bbc3f632a 33526104 papirus-icon-theme_20240411.orig.tar.gz
 89a6ad866a8411f46865885068418ae5c2491d6e 2260 papirus-icon-theme_20240411-1.debian.tar.xz
Checksums-Sha256:
 fb32245ce92630193759cb52c9bd1c05da33324ffb44ee597a0beca4a665df33 33526104 papirus-icon-theme_20240411.orig.tar.gz
 55778bd813d2f9d0110b9500dce5ebba2132b400c9db91974f5a3a19384e20aa 2260 papirus-icon-theme_20240411-1.debian.tar.xz
Files:
 e3b979d3b4ad6203137f2e3892fb24df 33526104 papirus-icon-theme_20240411.orig.tar.gz
 5e40e0eeb3d9df3a0ad678be93fe770a 2260 papirus-icon-theme_20240411-1.debian.tar.xz
