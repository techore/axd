Format: 3.0 (quilt)
Source: pam-rundir
Binary: pam-rundir
Architecture: any
Version: 0.0+git20210920.5338dec-1
Maintainer: techore <axdusk@pm.me>
Homepage: http://jjacky.com/pam_rundir/
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), libpam0g-dev
Package-List:
 pam-rundir deb x11 optional arch=any
Checksums-Sha1:
 a224bbb68c6eb3c664cf9067e51d216df3c5aab6 44337 pam-rundir_0.0+git20210920.5338dec.orig.tar.gz
 a4495c03e36f4d386bd177caac61c72966cd278f 1512 pam-rundir_0.0+git20210920.5338dec-1.debian.tar.xz
Checksums-Sha256:
 935c2bd590317b96b6c81b023471fd322bdf5bd86b0df32734db4a5a8029954d 44337 pam-rundir_0.0+git20210920.5338dec.orig.tar.gz
 d7406def8f5c8266c9983ad638b675ad276731fe1d86617a4c661b6bc9516cf0 1512 pam-rundir_0.0+git20210920.5338dec-1.debian.tar.xz
Files:
 b0678e0ac7d95e3eb39b3d87b202a3ea 44337 pam-rundir_0.0+git20210920.5338dec.orig.tar.gz
 60b316b20d3b52593071556ecd288e26 1512 pam-rundir_0.0+git20210920.5338dec-1.debian.tar.xz
