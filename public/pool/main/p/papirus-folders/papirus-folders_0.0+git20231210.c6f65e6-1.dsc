Format: 3.0 (quilt)
Source: papirus-folders
Binary: papirus-folders
Architecture: all
Version: 0.0+git20231210.c6f65e6-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://github.com/PapirusDevelopmentTeam/papirus-folders
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13)
Package-List:
 papirus-folders deb x11 optional arch=all
Checksums-Sha1:
 e16efa5d066223f1dfa16f9b2e0caec5a464acc9 9591 papirus-folders_0.0+git20231210.c6f65e6.orig.tar.gz
 8511b4c285d92c202d1a89d747d075308fdc3a03 2040 papirus-folders_0.0+git20231210.c6f65e6-1.debian.tar.xz
Checksums-Sha256:
 ba66c7e081ca9b1406db5cf9581c14e4e01801317b9841c0b6b153005bd37834 9591 papirus-folders_0.0+git20231210.c6f65e6.orig.tar.gz
 d7e5aea374d3d30835d9ed3ae961889c3e986b378325af7aab9663d9d9b43660 2040 papirus-folders_0.0+git20231210.c6f65e6-1.debian.tar.xz
Files:
 e86e2d1271256240b8f98856e5382e84 9591 papirus-folders_0.0+git20231210.c6f65e6.orig.tar.gz
 8baa132794bb392762e9249c21d38ee2 2040 papirus-folders_0.0+git20231210.c6f65e6-1.debian.tar.xz
