Format: 3.0 (quilt)
Source: slstatus
Binary: slstatus
Architecture: any
Version: 0.0+git20230428.fb334a1-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/slstatus
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libx11-dev
Package-List:
 slstatus deb x11 optional arch=any
Checksums-Sha1:
 567c66669482c23197da53b6d12d676e372eabf1 19624 slstatus_0.0+git20230428.fb334a1.orig.tar.gz
 a69efd59336895f43b798d2a00712f9ecbacd9b6 3448 slstatus_0.0+git20230428.fb334a1-1.debian.tar.xz
Checksums-Sha256:
 8a3afb4ba352f4618231b2f52b87aeaaefa3b427f1b9e15e14b7fba7b3247ddc 19624 slstatus_0.0+git20230428.fb334a1.orig.tar.gz
 43285f4eca34a9af161c88a0ebf720bb470444244dd88d23dd0da4520efe7612 3448 slstatus_0.0+git20230428.fb334a1-1.debian.tar.xz
Files:
 06af18dc54ed638da8b80d6fc787ad70 19624 slstatus_0.0+git20230428.fb334a1.orig.tar.gz
 949df958beefb48c6f81f5f9b4c5dcc8 3448 slstatus_0.0+git20230428.fb334a1-1.debian.tar.xz
