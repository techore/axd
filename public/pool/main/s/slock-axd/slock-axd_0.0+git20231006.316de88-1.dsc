Format: 3.0 (quilt)
Source: slock-axd
Binary: slock-axd
Architecture: any
Version: 0.0+git20231006.316de88-1
Maintainer: techore <axdusk@pm.me>
Homepage: https://gitlab.com/techore/slock-axd
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libx11-dev, libpam0g-dev
Package-List:
 slock-axd deb x11 optional arch=any
Checksums-Sha1:
 29879eb8a4a715fe4a2181d94aa18341d8bf6781 18745 slock-axd_0.0+git20231006.316de88.orig.tar.gz
 0ca9999b7628fb50d67f371680a88dccbeb12ede 5508 slock-axd_0.0+git20231006.316de88-1.debian.tar.xz
Checksums-Sha256:
 dde19470c5c3e61957aa2258516b00fb7441c2b91410b2f9f5734910f515714b 18745 slock-axd_0.0+git20231006.316de88.orig.tar.gz
 752bf7a9ecccefef1fc59a51fee89cc386a3984953bf81c19fa7abe9fc293ad1 5508 slock-axd_0.0+git20231006.316de88-1.debian.tar.xz
Files:
 b668656fce5c5aee916b2e92951368be 18745 slock-axd_0.0+git20231006.316de88.orig.tar.gz
 543ea8cfdd05e82653930f6a6bc94b1a 5508 slock-axd_0.0+git20231006.316de88-1.debian.tar.xz
